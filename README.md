#WordRank
WordRank is a word embedding algorithm that estimates vector representations for words via robust ranking. Similar to GloVe, WordRank's training is performed on aggregated word-word co-occurrence matrix from a corpus. But dissimilar to GloVe, where a regression loss is employed, WordRank optimizes a ranking-based loss. WordRank distributes computation across multiple machines via MPI to support large scale word embedding problems.

##License
All source code files in WordRank is under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0), except `./CMake/FindMKL.cmake` which is adopted from https://github.com/Eyescale/CMake and is under BSD.

##Prerequisites
WordRank is developed and tested on UNIX-based systems, with the following software dependencies:

- C++ compiler with C++11 support ([Intel compiler](https://software.intel.com/en-us/qualify-for-free-software) is preferred; g++ is ok but all #pragma simd are ignored as of now, which lead to 2x-3x performance loss.)
- MPI library, with multi-threading support (Intel MPI, MPICH2 or MVAPICH2)
- OpenMP
- CMake (at least 2.6)
- Boost library (at least 1.49)
- [GloVe v.1.0](http://nlp.stanford.edu/projects/glove/) (for co-occurrence matrix preparation)
- [HyperWords](https://bitbucket.org/omerlevy/hyperwords) (for evaluation)
- MKL (optional)

##Quick Start
1. Download the code: ```git clone https://bitbucket.org/shihaoji/wordrank```
2. Run ```.\install.sh``` to build the package (e.g., it downloads GloVe v.1.0 and HyperWords and applies patches to them, and then compiles the source code. Intel compiler is used as default. See the switch in .\install.sh to use g++ instead.)
3. Run the demo script: ```cd scripts; ./demo.sh```
4. Evaluate the models: ```cd scripts; ./eval.sh N (to evaluate the model after N iterations, e.g., N=200)```

##Reference
Shihao Ji, Hyokun Yun, Pinar Yanardag, Shin Matsushima, S. V. N. Vishwanathan. "[WordRank: Learning Word Embeddings via Robust Ranking](http://arxiv.org/abs/1506.02761)", arXiv preprint arxiv:1506.02761, 2015.